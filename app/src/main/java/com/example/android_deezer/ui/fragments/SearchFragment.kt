package com.example.android_deezer.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import com.example.android_deezer.R
import com.example.android_deezer.databinding.FragmentMenuBinding
import com.example.android_deezer.databinding.FragmentSearchBinding

class SearchFragment : Fragment() {
    private lateinit var binding: FragmentSearchBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSearchBinding.inflate(inflater,container, false)
        binding.linearLayout.isVisible = true
        binding.progressBar.isVisible = false
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.searchTitleButton.setOnClickListener {
            binding.linearLayout.isVisible = false
            binding.progressBar.isVisible = true
            val title = binding.musicTitle.text
            val action = SearchFragmentDirections.actionSearchFragmentToMenuFragment(title.toString())
            findNavController().navigate(action)
        }
    }
}