package com.example.android_deezer.data.api

import android.util.Log
import com.example.android_deezer.data.dto.toListSong
import com.example.android_deezer.data.dto.toSong
import com.example.android_deezer.domain.models.Song
import com.example.android_deezer.domain.models.Songs

class DeezerApiService {
    private val retrofit = RetrofitProvider.getRetrofit()

    suspend fun getSongs(title:String):List<Songs>{
        val response = retrofit.create(DeezerApi::class.java).GetSongs(title)
        //Control de error
        if(!response.isSuccessful){
            Log.e("ERROR", response.errorBody().toString())
        }

        return response.body()?.toListSong()?: emptyList()
    }

    suspend fun getSongDetail(id:Int):Song?{
        val response = retrofit.create(DeezerApi::class.java).GetSong(id)

        if(!response.isSuccessful){
            Log.e("ERROR", response.errorBody().toString())
        }

        return response.body()?.toSong()
    }
}