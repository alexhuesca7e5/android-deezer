package com.example.android_deezer.data.api

import com.example.android_deezer.data.dto.SongDto
import com.example.android_deezer.data.dto.SongsDto
import com.example.android_deezer.domain.models.Songs
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface DeezerApi {
    @GET("search/track")
    suspend fun GetSongs(@Query("q") title:String):Response<SongsDto>

    @GET("track/{id}")
    suspend fun GetSong(@Path("id") id:Int):Response<SongDto>
}