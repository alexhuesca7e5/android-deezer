package com.example.android_deezer.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android_deezer.*
import com.example.android_deezer.data.api.DeezerApiService
import com.example.android_deezer.databinding.FragmentMenuBinding
import com.example.android_deezer.domain.models.Songs
import com.example.android_deezer.ui.adapters.SongsViewAdapter
import kotlinx.coroutines.*
import kotlin.system.measureTimeMillis


class MenuFragment : Fragment() {
    //private lateinit var userAdapter: UserAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentMenuBinding
    private lateinit var songsViewAdapter: SongsViewAdapter
    val args: MenuFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentMenuBinding.inflate(inflater,container, false)
        binding.progressBar2.isVisible = false
        binding.recyclerView.isVisible = true
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //userAdapter = UserAdapter(getUsers(), this)
        //songsViewAdapter = SongsViewAdapter(getSongs(), onMusicClick)
        linearLayoutManager = LinearLayoutManager(context)

        //linearLayoutManager = GridLayoutManager(context, 3)

        val songs = getSongs()

        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = SongsViewAdapter(songs){
                onMusicClick(it)
            }
            //adapter = userAdapter
        }

    }

    private fun onMusicClick(songs: Songs){
        binding.recyclerView.isVisible = false
        binding.progressBar2.isVisible = true
        val action = MenuFragmentDirections.menuToDetail(songs.id)
        findNavController().navigate(action)
    }

    private fun getSongs(): List<Songs>{
        val service = DeezerApiService()
        var listOfSongs = listOf<Songs>()
        runBlocking {
            measureTimeMillis {
                val b = async { service.getSongs(args.title) }

                listOfSongs = b.await()
            }
        }

        return  listOfSongs
    }
}