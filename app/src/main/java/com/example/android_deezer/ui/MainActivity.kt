package com.example.android_deezer.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.android_deezer.R
import com.example.android_deezer.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_Androiddeezer)
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


//        supportFragmentManager.beginTransaction().apply {
//            replace(R.id.fragmentContainerView, MenuFragment())
//            setReorderingAllowed(true)
//            addToBackStack(null) // name can be null
//            commit()
//        }
    }
}