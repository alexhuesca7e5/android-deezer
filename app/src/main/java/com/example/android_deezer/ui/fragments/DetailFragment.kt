package com.example.android_deezer.ui.fragments

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.android_deezer.R
import com.example.android_deezer.data.api.DeezerApiService
import com.example.android_deezer.databinding.FragmentDetailBinding
import com.example.android_deezer.domain.models.Song
import kotlinx.coroutines.Runnable
import kotlinx.coroutines.runBlocking
import kotlin.system.measureTimeMillis

class DetailFragment : Fragment() {
    lateinit var imagePlayPause:ImageView
    lateinit var textCurrentTime:TextView
    lateinit var textTotalDuration:TextView
    lateinit var playerSeekBar:SeekBar
    lateinit var mediaPlayer:MediaPlayer
    lateinit var  handler: Handler
    lateinit var binding: FragmentDetailBinding
    val args: DetailFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentDetailBinding.inflate(inflater,container, false)
        return binding.root
    }
    @SuppressLint("SetTextI18n", "ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val song = getSong()

        handler = Handler()

        binding.textViewName.text = "NAME:\n${song?.title}"
        binding.textViewArtist.text = "ARTIST:\n${song?.artist}"
        binding.textViewAlbum.text = "ALBUM:\n${song?.album}"
        binding.textReleaseDate.text = "RELEASE DATE:\n${song?.release_date}"
        Glide.with(this)
            .load(song?.image)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .circleCrop()
            .into(binding.imgPhoto)

        imagePlayPause = binding.playButton
        textCurrentTime = binding.textCurrentTime
        textTotalDuration = binding.textTotalDuration
        playerSeekBar = binding.playerSeekBar
        mediaPlayer = MediaPlayer()

        playerSeekBar.max = 100

        imagePlayPause.setOnClickListener(View.OnClickListener(){
            if (mediaPlayer.isPlaying){
                handler.removeCallbacks(updater)
                mediaPlayer.pause()
                imagePlayPause.setImageResource(R.drawable.ic_play)
            } else {
                mediaPlayer.start()
                imagePlayPause.setImageResource(R.drawable.ic_pause)
                updateSeekBar()
            }
        })

        prepareMediaPlayer()

        playerSeekBar.setOnTouchListener { view, motionEvent ->
            val seekBar:SeekBar = view as SeekBar
            val playPosition = (mediaPlayer.duration / 100) * seekBar.progress
            mediaPlayer.seekTo(playPosition)
            textCurrentTime.setText(jiji(mediaPlayer.currentPosition))
            return@setOnTouchListener false
        }

        mediaPlayer.setOnCompletionListener {
            playerSeekBar.progress = 0
            imagePlayPause.setImageResource(R.drawable.ic_play)
            textCurrentTime.setText(R.string.zero)
            textTotalDuration.setText(R.string.zero)
            mediaPlayer.stop()
            mediaPlayer.reset()
            prepareMediaPlayer()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        mediaPlayer.stop()
    }

    private fun prepareMediaPlayer(){
        val song = getSong()
        try {
            if (song != null) {
                mediaPlayer.setDataSource(song.preview)
                mediaPlayer.prepare()
                textTotalDuration.text = jiji(mediaPlayer.duration)
            }
        } catch (exception:Exception){
            Toast.makeText(context, exception.message, Toast.LENGTH_SHORT).show()
        }
    }

    private val updater = Runnable {
        updateSeekBar()
        val currentDuration: Int = mediaPlayer.currentPosition
        Log.e("TIMER", currentDuration.toString())
        textCurrentTime.text = jiji(currentDuration)
    }

    private fun updateSeekBar(){
        val tmp: Float = (0f + mediaPlayer.currentPosition)/30000
        if (mediaPlayer.isPlaying){
            playerSeekBar.progress = Math.round(tmp*100)
            handler.postDelayed(updater, 1000)
        }
    }

    private fun jiji(milisegundos: Int): String {
        val segundos = milisegundos / 1000
        val minutos = segundos / 60
        return String.format("%01d:%02d", minutos, segundos % 60)
    }


    private fun getSong(): Song?{

        val service = DeezerApiService()

        var result:Song? = null

        runBlocking {

            measureTimeMillis {
                result = service.getSongDetail(args.songID)
            }
        }

        return  result
    }
}