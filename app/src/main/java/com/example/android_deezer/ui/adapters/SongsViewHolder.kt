package com.example.android_deezer.ui.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.android_deezer.databinding.ItemUserBinding
import com.example.android_deezer.domain.models.Songs

class SongsViewHolder(view:View):RecyclerView.ViewHolder(view) {
    val binding = ItemUserBinding.bind(view)

    fun draw(songs: Songs, onClickListener:(Songs)->Unit){
        binding.textViewName.text = songs.title
        Glide.with(binding.imgPhoto.context)
            .load(songs.image)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop()
            .circleCrop()
            .into(binding.imgPhoto)
        itemView.setOnClickListener{
            onClickListener(songs)
        }
    }
}
