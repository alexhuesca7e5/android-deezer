package com.example.android_deezer.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.android_deezer.R
import com.example.android_deezer.domain.models.Songs

class SongsViewAdapter(private val songList: List<Songs>, private val onClickListener:(Songs)-> Unit):RecyclerView.Adapter<SongsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SongsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return SongsViewHolder(layoutInflater.inflate(R.layout.item_user,parent,false))
    }

    override fun onBindViewHolder(holder: SongsViewHolder, position: Int) {
        val item = songList[position]
        holder.draw(item,onClickListener)
    }

    override fun getItemCount(): Int = songList.size
}