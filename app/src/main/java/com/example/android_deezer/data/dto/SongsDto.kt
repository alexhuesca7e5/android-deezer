package com.example.android_deezer.data.dto

import com.example.android_deezer.domain.models.Songs

data class SongsDto(
    val data: List<Data>,
    val next: String,
    val total: Int
)

fun SongsDto.toListSong():List<Songs>{
    val result = data.mapIndexed { index, data ->
        Songs(
            id = data.id,
            title = data.title,
            image = data.album.cover_medium
        )
    }
    return result
}