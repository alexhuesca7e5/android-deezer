package com.example.android_deezer.data.dto

import com.example.android_deezer.domain.models.Song

data class SongDto(
    val album: AlbumDetail,
    val artist: ArtistDetail,
    val available_countries: List<String>,
    val bpm: Double,
    val contributors: List<Contributor>,
    val disk_number: Int,
    val duration: Int,
    val explicit_content_cover: Int,
    val explicit_content_lyrics: Int,
    val explicit_lyrics: Boolean,
    val gain: Double,
    val id: Int,
    val isrc: String,
    val link: String,
    val md5_image: String,
    val preview: String,
    val rank: Int,
    val readable: Boolean,
    val release_date: String,
    val share: String,
    val title: String,
    val title_short: String,
    val title_version: String,
    val track_position: Int,
    val type: String
)

fun SongDto.toSong(): Song{
    val result = Song(
        id = id,
        title = title,
        artist = artist.name,
        album = album.title,
        image = artist.picture_medium,
        release_date = release_date,
        preview = preview
    )
    return result
}