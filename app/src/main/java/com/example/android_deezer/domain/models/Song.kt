package com.example.android_deezer.domain.models

data class Song(val id:Int,
                val title:String,
                val artist:String,
                val album:String,
                val image:String,
                val release_date:String,
                val preview:String
                )
